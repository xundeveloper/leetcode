package binarysearch

import (
	"math"
	"sort"
)

/*
The distance of a pair of integers a and b is defined as the absolute difference between a and b.

Given an integer array nums and an integer k, return the kth smallest distance among all the pairs nums[i] and nums[j] where 0 <= i < j < nums.length.

Example 1:
Input: nums = [1,3,1], k = 1
Output: 0
Explanation: Here are all the pairs:
(1,3) -> 2
(1,1) -> 0
(3,1) -> 2
Then the 1st smallest distance pair is (1,1), and its distance is 0.

Example 2:
Input: nums = [1,1,1], k = 2
Output: 0

Example 3:
Input: nums = [1,6,1], k = 3
Output: 5

Constraints:
n == nums.length
2 <= n <= 104
0 <= nums[i] <= 106
1 <= k <= n * (n - 1) / 2

star: *****
*/

func smallestDistancePair719(nums []int, k int) int {
	sort.Ints(nums)
	n := len(nums)

	// find min difference
	low := nums[1] - nums[0]
	for i := 1; i < n-1; i++ {
		low = int(math.Min(float64(low), float64(nums[i+1]-nums[i])))
	}

	// max difference
	high := nums[n-1] - nums[0]

	// do binary search for k-th absolute difference
	for low < high {
		mid := low + (high-low)/2
		if countPairs(nums, mid) < k {
			low = mid + 1
		} else {
			high = mid
		}

	}

	return low
}

// Returns number of pairs with absolute difference less than or equal to mid.
func countPairs(nums []int, mid int) int {
	res := 0
	for i := 0; i < len(nums); i++ {
		res += upperBound(nums, i, len(nums)-1, nums[i]+mid) - i - 1
	}
	return res
}

// Returns index of first index of element which is greater than key
func upperBound(nums []int, low, high, key int) int {
	if nums[high] <= key {
		return high + 1
	}

	for low < high {
		mid := low + (high-low)/2
		if key >= nums[mid] {
			low = mid + 1
		} else {
			high = mid
		}
	}

	return low
}
