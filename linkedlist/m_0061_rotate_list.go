package linkedlist

/*
	Given the head of a linked list, rotate the list to the right by k places.

	Example 1:
	Input: head = [1,2,3,4,5], k = 2
	Output: [4,5,1,2,3]

	Example 2:
	Input: head = [0,1,2], k = 4
	Output: [2,0,1]

	Constraints:
	The number of nodes in the list is in the range [0, 500].
	-100 <= Node.val <= 100
	0 <= k <= 2 * 109

	Solution: normal traversal to get length
	:(
*/

func rotateRight0061(head *ListNode, k int) *ListNode {
	// return head if only one node
	if head == nil || head.Next == nil {
		return head
	}
	// get the linklist length

	prev, current, len := head.Next, head.Next.Next, 2
	for current != nil {
		len++
		prev, current = current, current.Next
	}
	// return head if k % len = 0, we don't need to rotate
	if k%len == 0 {
		return head
	}
	// regular case
	current, targetIndex := head, len-(k%len)
	for i := 0; i < targetIndex-1; i++ {
		current = current.Next
	}
	newHead := current.Next
	current.Next, prev.Next = nil, head
	return newHead
}
