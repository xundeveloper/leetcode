package linkedlist

/*
	Given the head of a sorted linked list, delete all duplicates such that each element
	appears only once. Return the linked list sorted as well.

	Example 1:
	Input: head = [1,1,2]
	Output: [1,2]

	Example 2:
	Input: head = [1,1,2,3,3]
	Output: [1,2,3]

	Constraints:
	The number of nodes in the list is in the range [0, 300].
	-100 <= Node.val <= 100
	The list is guaranteed to be sorted in ascending order.

	Test: [1,1,2], [1,1,2,3,3], [1,1,1]
*/

func deleteDuplicates0083(head *ListNode) *ListNode {
	prev, current := head, head

	for current != nil {
		if prev.Val == current.Val && prev != current {
			current, prev.Next = current.Next, current.Next
			continue
		}
		prev, current = current, current.Next
	}

	return head
}
