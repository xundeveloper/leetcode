package linkedlist

/*
	Given the head of a sorted linked list, delete all nodes that have duplicate numbers,
	leaving only distinct numbers from the original list. Return the linked list sorted as well.

	Example 1:
	Input: head = [1,2,3,3,4,4,5]
	Output: [1,2,5]

	Example 2:
	Input: head = [1,1,1,2,3]
	Output: [2,3]


	Constraints:
	The number of nodes in the list is in the range [0, 300].
	-100 <= Node.val <= 100
	The list is guaranteed to be sorted in ascending order.

	time consuming: 5 stars
*/

func deleteDuplicates82(head *ListNode) *ListNode {
	if head.Next == nil {
		return head
	}

	var unique *ListNode
	current := head

	for current != nil {
		nextNewNode, isDuplicated := helper82(current, 1)
		if !isDuplicated {
			if head == nil {
				head = current // edge case: if head node was set to nil, we will have to reassign it to the 1st non-duplicated node
			}
			if unique == nil {
				unique = current
			} else {
				unique.Next, unique = current, current
			}
		} else {
			if current == head { // edge case: when head node is duplicated, we temporaily set head to nil
				head = nil
			}
			if unique != nil {
				unique.Next = nil // if current node is duplicated, then set unique's next node to nil
			}
		}
		current = nextNewNode
	}

	return head
}

/*
	to get find next node that is NOT equal to your current node value
	and return a boolean flag representing if current node value is duplicated or not
*/
func helper82(current *ListNode, count int) (*ListNode, bool) {
	if current.Next == nil {
		return nil, count > 1
	}

	if current.Val != current.Next.Val {
		return current.Next, count > 1
	}

	return helper82(current.Next, count+1)
}
