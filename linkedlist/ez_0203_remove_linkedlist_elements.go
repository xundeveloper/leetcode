package linkedlist

/*
	Given the head of a linked list and an integer val, remove all the nodes of the linked list that has Node.val == val, and return the new head.

	Example 1:
	Input: head = [1,2,6,3,4,5,6], val = 6
	Output: [1,2,3,4,5]

	Example 2:
	Input: head = [], val = 1
	Output: []

	Example 3:
	Input: head = [7,7,7,7], val = 7
	Output: []

	Example 4:
	Input: [1,2,2,1]
	Output: 2
*/

// [1,2,2,1] 2
func removeElements0203(head *ListNode, val int) *ListNode {
	var prev *ListNode
	current := head

	for current != nil {
		if current.Val == val {
			if prev == nil {
				head = current.Next
			} else {
				prev.Next = current.Next
			}
			current = current.Next
		} else {
			prev, current = current, current.Next
		}
	}

	return head
}
