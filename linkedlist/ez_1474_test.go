package linkedlist

import (
	"testing"
)

type testData struct {
	m, n int
	head *ListNode
}

func TestDeleteNodes1474(t *testing.T) {
	inputs := []*testData{
		{
			m:    2,
			n:    3,
			head: addNumbersToLinkedList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13),
		},
		{
			m:    1,
			n:    3,
			head: addNumbersToLinkedList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11),
		},
	}
	expects := []*ListNode{
		addNumbersToLinkedList(1, 2, 6, 7, 11, 12),
		addNumbersToLinkedList(1, 5, 9),
	}

	actuals := []*ListNode{}
	for _, input := range inputs {
		actual := deleteNodes1474(input.head, input.m, input.n)
		actuals = append(actuals, actual)
	}

	for index, actual := range actuals {
		if expect := expects[index]; expect.Val != actual.Val {
			t.Errorf("Expected %d but actual: %d", expect.Val, actual.Val)
		}
	}
}
