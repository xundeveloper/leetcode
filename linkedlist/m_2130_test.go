package linkedlist

import "testing"

func Test2130(t *testing.T) {
	inputs := []*ListNode{
		addNumbersToLinkedList(1, 100),
		addNumbersToLinkedList(5, 4, 2, 1),
		addNumbersToLinkedList(4, 2, 2, 3),
	}
	expects := []int{101, 6, 7}
	for index, input := range inputs {
		output, expect := pairSum2130(input), expects[index]
		if output != expect {
			t.Errorf("Expected %d but actual: %d", expect, output)
		}
	}
}
