package linkedlist

import "log"

type ImmutableListNode interface {
	GetNext() ImmutableListNode
	PrintValue()
}

func (node *ListNode) GetNext() ImmutableListNode {
	var next interface{} = node.Next
	return next.(ImmutableListNode)
}

func (node *ListNode) PrintValue() {
	log.Println(node.Val)
}

/*
	Example 1:
	Input: head = [1,2,3,4]
	Output: [4,3,2,1]

	Example 2:
	Input: head = [0,-4,-1,3,-5]
	Output: [-5,3,-1,-4,0]

	Example 3:
	Input: head = [-2,0,6,4,4,-6]
	Output: [-6,4,4,6,0,-2]

	Follow up:
	Could you solve this problem in:
	1. Constant space complexity?
	2. Linear time complexity and less than linear space complexity?
*/

func printLinkedListInReverse1265(head ImmutableListNode) {

}
