package linkedlist

/*
You are given an array of k linked-lists lists, each linked-list is sorted in ascending order.

Merge all the linked-lists into one sorted linked-list and return it.

Example 1:
Input: lists = [[1,4,5],[1,3,4],[2,6]]
Output: [1,1,2,3,4,4,5,6]
Explanation: The linked-lists are:
[
  1->4->5,
  1->3->4,
  2->6
]
merging them into one sorted list:
1->1->2->3->4->4->5->6

Example 2:
Input: lists = []
Output: []
Example 3:

Input: lists = [[]]
Output: []

Constraints:
k == lists.length
0 <= k <= 104
0 <= lists[i].length <= 500
-104 <= lists[i][j] <= 104
lists[i] is sorted in ascending order.
The sum of lists[i].length will not exceed 104.
*/

func mergeKLists23(lists []*ListNode) *ListNode {
	var res, current, minNode *ListNode

	index, minIndex, nilNodeNum := 0, 0, 0

	for index < len(lists) {
		node := lists[index]
		if node == nil {
			nilNodeNum++
			if nilNodeNum == len(lists) {
				break
			}
		} else {
			if minNode == nil || node.Val <= minNode.Val {
				minNode, minIndex = node, index
			}
		}
		index++
		if index == len(lists) {
			if res == nil {
				res, minNode.Next, lists[minIndex] = minNode, nil, minNode.Next
				current = res
			} else {
				current.Next, minNode.Next, lists[minIndex] = minNode, nil, minNode.Next
				current = current.Next
			}
			minNode, minIndex, index, nilNodeNum = nil, 0, 0, 0 // important: reset all properties
		}
	}

	return res
}
