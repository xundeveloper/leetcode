package linkedlist

import "testing"

func Test1290(t *testing.T) {
	inputs := []*ListNode{
		addNumbersToLinkedList(1, 0, 1),
		addNumbersToLinkedList(0),
	}
	expects := []int{5, 0}

	for index, input := range inputs {
		actual := getDecimalValue1290(input)
		if expect := expects[index]; expect != actual {
			t.Errorf("Expected:%d\nActual:%d", expect, actual)
		}
	}
}
