package linkedlist

/*

Example 1:
Input: head = [1,2,3,4,5,6,7,8,9,10,11,12,13], m = 2, n = 3
Output: [1,2,6,7,11,12]

Example 2:
Input: head = [1,2,3,4,5,6,7,8,9,10,11], m = 1, n = 3
Output: [1,5,9]

Constraints:
The number of nodes in the list is in the range [1, 104].
1 <= Node.val <= 106
1 <= m, n <= 1000

*/

func deleteNodes1474(head *ListNode, m int, n int) *ListNode {
	var beforeDeletion *ListNode
	current, start, steps := head, 0, 1
	for current != nil {
		if start < m {
			start++
			if start == m { // check if the next node is the 1st node to delete
				// we need to update the node right before the 1st node to delete
				beforeDeletion = current
			}
		} else {
			// perform the deletion
			if steps < n {
				steps++
				if current.Next == nil { // check if the current node is the tail
					beforeDeletion.Next = nil // delete all node from the beforeDeletion
				}
			} else {
				start, steps = 0, 1
				beforeDeletion.Next = current.Next
			}
		}
		current = current.Next
	}
	return head
}
