package linkedlist

/*
	You are given the heads of two sorted linked lists list1 and list2.
	Merge the two lists in a one sorted list. The list should be made by splicing together the nodes of the first two lists.
	Return the head of the merged linked list.

	Example 1:
	Input: list1 = [1,2,4], list2 = [1,3,4]
	Output: [1,1,2,3,4,4]

	Example 2:
	Input: list1 = [], list2 = []
	Output: []

	Example 3:
	Input: list1 = [], list2 = [0]
	Output: [0]

	Constraints:
	The number of nodes in both lists is in the range [0, 50].
	-100 <= Node.val <= 100
	Both list1 and list2 are sorted in non-decreasing order.
*/

func mergeTwoLists0021(list1 *ListNode, list2 *ListNode) *ListNode {
	var res, resCurrent *ListNode
	current1, current2 := list1, list2

	for current1 != nil || current2 != nil {
		var val int
		if current1 == nil {
			val, current2 = current2.Val, current2.Next
		} else if current2 == nil {
			val, current1 = current1.Val, current1.Next
		} else {
			if current1.Val < current2.Val {
				val, current1 = current1.Val, current1.Next
			} else {
				val, current2 = current2.Val, current2.Next
			}
		}

		if res == nil {
			res = &ListNode{Val: val}
			resCurrent = res
		} else {
			resCurrent.Next = &ListNode{Val: val}
			resCurrent = resCurrent.Next
		}
	}

	return res
}
