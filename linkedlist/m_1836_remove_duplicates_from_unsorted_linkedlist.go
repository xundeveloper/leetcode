package linkedlist

/*
	Given the head of a linked list, find all the values that appear more
	than once in the list and delete the nodes that have any of those values.

	Example 1:
	Input: head = [1,2,3,2]
	Output: [1,3]
	Explanation: 2 appears twice in the linked list, so all 2's should be deleted. After deleting all 2's, we are left with [1,3].

	Example 2:
	Input: head = [2,1,1,2]
	Output: []
	Explanation: 2 and 1 both appear twice. All the elements should be deleted.

	Example 3:
	Input: head = [3,2,2,1,3,2,4]
	Output: [1,4]
	Explanation: 3 appears twice and 2 appears three times. After deleting all 3's and 2's, we are left with [1,4].


	Constraints:
	The number of nodes in the list is in the range [1, 105]
	1 <= Node.val <= 105
*/

func deleteDuplicatesUnsorted1836(head *ListNode) *ListNode {
	var prev *ListNode
	current, cache := head, make([]int, 1000000)

	for current != nil {
		val := current.Val
		if cache[val] == 1 {
			prev.Next, current = current.Next, current.Next
		} else {
			prev, current = current, current.Next
		}
		cache[val]++
	}

	prev, current = nil, head
	for current != nil {
		val := current.Val
		if cache[val] > 1 {
			if prev == nil {
				current, head = head.Next, head.Next
			} else {
				prev.Next, current = current.Next, current.Next
			}
		} else {
			prev, current = current, current.Next
		}
	}

	return head
}
