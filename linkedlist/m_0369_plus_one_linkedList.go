package linkedlist

/*
Given a non-negative integer represented as a linked list of digits, plus one to the integer.

The digits are stored such that the most significant digit is at the head of the list.

Example 1:
Input: head = [1,2,3]
Output: [1,2,4]

Example 2:
Input: head = [0]
Output: [1]

Constraints:
The number of nodes in the linked list is in the range [1, 100].
0 <= Node.val <= 9
The number represented by the linked list does not contain leading zeros except for the zero itself.
*/

func plusOne369(head *ListNode) *ListNode {
	stack := make([]int, 0, 100)
	current := head
	for current != nil {
		stack = append(stack, current.Val)
		current = current.Next
	}

	var res *ListNode
	carry := 0

	for len(stack) > 0 {
		sum := stack[len(stack)-1] + carry
		stack = stack[:len(stack)-1]
		carry = 0

		if res == nil {
			sum++
		}

		if sum == 10 {
			sum = 0
			carry++
		}

		if res == nil {
			res = &ListNode{Val: sum}
		} else {
			res = &ListNode{
				Val:  sum,
				Next: res,
			}
		}
	}

	if res.Val == 0 {
		res = &ListNode{
			Val:  1,
			Next: res,
		}
	}

	return res
}
