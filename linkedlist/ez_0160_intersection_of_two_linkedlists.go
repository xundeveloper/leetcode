package linkedlist

import "math"

/*
	Given the heads of two singly linked-lists headA and headB, return the node at
	which the two lists intersect. If the two linked lists have no intersection at
	all, return null.
*/

func getIntersectionNode0160(headA, headB *ListNode) *ListNode {
	currentA, currentB := headA, headB
	sizeA, sizeB := 0, 0
	for currentA != nil || currentB != nil {
		if currentA != nil {
			currentA = currentA.Next
			sizeA++
		}
		if currentB != nil {
			currentB = currentB.Next
			sizeB++
		}
	}

	gap := int(math.Abs(float64(sizeA - sizeB)))

	currentA, currentB = headA, headB

	for i := 0; i < gap; i++ {
		if sizeA < sizeB {
			currentB = currentB.Next
		} else {
			currentA = currentA.Next
		}
	}

	for currentA != nil {
		if currentA == currentB {
			return currentA
		}
		currentA, currentB = currentA.Next, currentB.Next
	}

	return nil
}

// apprach 2: we can use hash table as well
