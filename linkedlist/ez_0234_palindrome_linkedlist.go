package linkedlist

/*
	Given the head of a singly linked list, return true if it is a palindrome.

	Example 1:
	Input: head = [1,2,2,1]
	Output: true

	Example 2:
	Input: head = [1,2]
	Output: false

	Constraints:
	The number of nodes in the list is in the range [1, 105].
	0 <= Node.val <= 9
*/

func isPalindrome0234(head *ListNode) bool {
	fast, slow := head, head
	// find the mid node
	for fast != nil {
		fast = fast.Next
		if fast == nil {
			slow = slow.Next
			break
		}
		fast, slow = fast.Next, slow.Next
	}

	// reverse the second half of linkedlist after the mid node
	current, reverse := head, reverseLinkedList(slow)

	// compare the 2nd with the 1st half
	for current != nil || reverse != nil {
		if current == nil || reverse == nil {
			return true
		}
		if current.Val != reverse.Val {
			return false
		}
		current, reverse = current.Next, reverse.Next
	}

	return true
}
