package linkedlist

/*
	Given the head of a linked list, remove the nth node from the end of the list and return its head.

	Example 1:
	Input: head = [1,2,3,4,5], n = 2
	Output: [1,2,3,5]

	Example 2:
	Input: head = [1], n = 1
	Output: []

	Example 3:
	Input: head = [1,2], n = 2
	Output: [0]

	Constraints:
	The number of nodes in the list is sz.
	1 <= sz <= 30
	0 <= Node.val <= 100
	1 <= n <= sz

	Solution: normal traversal to get the length
*/

func removeNthFromEnd0019(head *ListNode, n int) *ListNode {
	// traverse the list to get the length
	len, current := 0, head
	for current != nil {
		len++
		current = current.Next
	}

	var prev *ListNode
	current, target := head, len-n // now we know the targe is len - n

	for i := 0; i < target; i++ {
		prev, current = current, current.Next
	}

	if prev != nil {
		prev.Next = current.Next
		return head
	}

	return head.Next
}
