package linkedlist

import "math"

/*
You are given the head of a linked list, and an integer k.

Return the head of the linked list after swapping the values of the kth node from the beginning and the kth node from the end (the list is 1-indexed).

Example 1:
Input: head = [1,2,3,4,5], k = 2
Output: [1,4,3,2,5]

Example 2:
Input: head = [7,9,6,6,7,8,3,0,9,5], k = 5
Output: [7,9,6,6,8,7,3,0,9,5]

Constraints:
The number of nodes in the list is n.
1 <= k <= n <= 105
0 <= Node.val <= 100
*/

func swapNodes1721(head *ListNode, k int) *ListNode {
	current, size := head, 0

	for current != nil {
		size++
		current = current.Next
	}

	start, end := int(math.Min(float64(k), float64(size-k+1))), int(math.Max(float64(k), float64(size-k+1)))

	if start == end {
		return head
	}

	var targetStart, prevStart, prev *ListNode
	current, position := head, 1
	for current != nil {
		if position == start {
			targetStart = current
			if prev != nil {
				prevStart = prev
			}
		}

		if position == end {
			break
		}

		position++
		prev, current = current, current.Next
	}

	if prevStart == nil {
		if prev == targetStart {
			head.Next, current.Next, head = current.Next, head, current // [1,2] 1
		} else {
			head.Next, current.Next, prev.Next, head = current.Next, head.Next, head, current // [1,2,3] 1
		}
	} else {
		if prev == targetStart {
			prevStart.Next, targetStart.Next, current.Next = current, current.Next, targetStart // [1,2,3,4] 2
		} else {
			prevStart.Next, targetStart.Next, current.Next, prev.Next = current, current.Next, targetStart.Next, targetStart // normal case
		}
	}

	return head
}
