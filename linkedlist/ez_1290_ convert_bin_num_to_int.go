package linkedlist

import "math"

/*
	Example 1:
	Input: head = [1,0,1]
	Output: 5
	Explanation: (101) in base 2 = (5) in base 10

	Example 2:
	Input: head = [0]
	Output: 0

	Constraints:
	The Linked List is not empty.
	Number of nodes will not exceed 30.
	Each node's value is either 0 or 1.
*/

func getDecimalValue1290(head *ListNode) int {
	reversed := reverseLinkedList(head)
	res, bit, current := 0, 0, reversed

	for current != nil {
		addition := float64(0)
		if current.Val != 0 {
			addition = math.Pow(float64(2), float64(bit))
		}
		res += int(addition)
		bit++
		current = current.Next
	}

	return res
}
