package linkedlist

import "fmt"

func reverseLinkedList(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}

	var previous *ListNode
	current := head

	for current != nil {
		if current.Next == nil {
			current.Next = previous
			break
		}
		current.Next, current, previous = previous, current.Next, current
	}

	return current
}

func addNumbersToLinkedList(nums ...int) *ListNode {
	var res, current *ListNode
	for _, num := range nums {
		if res == nil {
			res = &ListNode{
				Val:  num,
				Next: nil,
			}
			current = res
			continue
		}
		current.Next = &ListNode{
			Val:  num,
			Next: nil,
		}
		current = current.Next
	}
	return res
}

func (node *ListNode) Print() {
	res := ""

	current := node

	for current != nil {
		if current.Next == nil {
			res += fmt.Sprintf("%d", current.Val)
		} else {
			res += fmt.Sprintf("%d -> ", current.Val)
		}
		current = current.Next
	}

	fmt.Println(res)
}

func areEqual(head1 *ListNode, head2 *ListNode) bool {
	if head1 == nil && head2 == nil {
		return true
	}

	if head1 == nil || head2 == nil {
		return false
	}

	current1, current2 := head1, head2

	for current1 != nil || current2 != nil {
		if current1 == nil || current2 == nil {
			return false
		}
		if current1.Val != current2.Val {
			return false
		}
		current1, current2 = current1.Next, current2.Next
	}

	return true
}
