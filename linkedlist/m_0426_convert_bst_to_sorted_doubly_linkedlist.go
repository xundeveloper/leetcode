package linkedlist

/*
Convert a Binary Search Tree to a sorted Circular Doubly-Linked List in place.

You can think of the left and right pointers as synonymous to the predecessor and successor pointers in a doubly-linked list.
For a circular doubly linked list, the predecessor of the first element is the last element, and the successor of the last element is the first element.

We want to do the transformation in place. After the transformation, the left pointer of the tree node should point to its predecessor, and the right
pointer should point to its successor. You should return the pointer to the smallest element of the linked list.

Example 1:
Input: root = [4,2,5,1,3]
Output: [1,2,3,4,5]
Explanation: The figure below shows the transformed BST. The solid line indicates the successor relationship, while the dashed line means the predecessor relationship.

Example 2:
Input: root = [2,1,3]
Output: [1,2,3]

Constraints:
The number of nodes in the tree is in the range [0, 2000].
-1000 <= Node.val <= 1000
All the values of the tree are unique.

VERY TIME CONSUMING
*/

func treeToDoublyList426(root *Node) *Node {
	if root == nil {
		return root
	}

	if root.Left == nil && root.Right == nil {
		root.Left, root.Right = root, root
		return root
	}

	min, max := helper426(root, root, root)

	start, end := min, max

	for start != root {
		start.Right.Left = start
		if start.Right == root {
			root.Left = start
		}
		start = start.Right
	}

	for end != root {
		end.Left.Right = end
		if end.Left == root {
			root.Right = end
		}
		end = end.Left
	}

	min.Left, max.Right = max, min
	return min
}

func helper426(maleft, miRight, root *Node) (*Node, *Node) {
	maxLeftParent, maxLeft := maxNode(nil, root.Left)
	minRightParent, minRight := minNode(nil, root.Right)

	if maxLeft == nil && minRight == nil {
		return maleft, miRight
	}

	if maxLeft != nil {
		maxLeft.Right = maleft // connect max left to root from left side
		if maxLeft == root.Left {
			// if max left is left child of root
			root.Left = maxLeft.Left
		}
		if maxLeftParent != nil {
			// if the parent of max left is not nil
			maxLeftParent.Right = maxLeft.Left
		}
		// remember to clear the left child of max left
		maxLeft.Left = nil
	} else {
		maxLeft = maleft
	}

	if minRight != nil {
		minRight.Left = miRight
		if minRight == root.Right {
			root.Right = minRight.Right
		}
		if minRightParent != nil {
			minRightParent.Left = minRight.Right
		}
		minRight.Right = nil
	} else {
		minRight = miRight
	}

	return helper426(maxLeft, minRight, root)
}

/*
	return max child node of current root node plus the parent of the max node
*/
func maxNode(parent, node *Node) (*Node, *Node) {
	if node == nil || node.Right == nil {
		return parent, node
	}

	return maxNode(node, node.Right)
}

/*
	return min child node of current root node plus the parent of the min node
*/
func minNode(parent, node *Node) (*Node, *Node) {
	if node == nil || node.Left == nil {
		return parent, node
	}

	return minNode(node, node.Left)
}
