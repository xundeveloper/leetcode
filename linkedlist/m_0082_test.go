package linkedlist

import (
	"testing"
)

func Test82(t *testing.T) {
	inputs := []*ListNode{
		addNumbersToLinkedList(1, 2, 3, 3, 4, 4, 5),
		addNumbersToLinkedList(1, 1, 1, 2),
		addNumbersToLinkedList(1, 1, 1),
		addNumbersToLinkedList(1, 2, 3),
		addNumbersToLinkedList(1, 1, 1, 2, 3),
	}
	expectedResults := []*ListNode{
		addNumbersToLinkedList(1, 2, 5),
		addNumbersToLinkedList(2),
		nil,
		addNumbersToLinkedList(1, 2, 3),
		addNumbersToLinkedList(2, 3),
	}
	for index, input := range inputs {
		actual := deleteDuplicates82(input)
		if !areEqual(expectedResults[index], actual) {
			t.Errorf("%d not equal expected", index)
		}
	}
}
