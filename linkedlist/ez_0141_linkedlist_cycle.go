package linkedlist

/*
	Given head, the head of a linked list, determine if the linked list has a cycle in it.
*/

func hasCycle0141(head *ListNode) bool {
	fast, slow := head, head

	for fast != nil {
		fast = fast.Next
		if fast == nil {
			return false
		}
		fast, slow = fast.Next, slow.Next
		if fast == slow {
			return true
		}
	}

	return false
}
