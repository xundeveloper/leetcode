package linkedlist

/*
	Given a linked list, swap every two adjacent nodes and return its head.
	You must solve the problem without modifying the values in the list's nodes
	(i.e., only nodes themselves may be changed.)

	Example 1:
	Input: head = [1,2,3,4]
	Output: [2,1,4,3]

	Example 2:
	Input: head = []
	Output: []

	Example 3:
	Input: head = [1]
	Output: [1]

	Example 4:
	Input: head = [1,2,3]
	Output: [2,1,3]

	Constraints:
	The number of nodes in the list is in the range [0, 100].
	0 <= Node.val <= 100

	Solution:
*/

func swapPairs0024(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}

	slow, quick := head, head.Next
	var tail *ListNode
	for quick != nil {
		slow.Next, quick.Next = quick.Next, slow
		if tail == nil {
			head = quick
		} else {
			tail.Next = quick
		}
		tail, slow = slow, slow.Next
		if slow == nil {
			break
		}
		quick = slow.Next
	}

	return head
}
