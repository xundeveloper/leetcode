package linkedlist

/*
You are given two non-empty linked lists representing two non-negative integers. The most significant
digit comes first and each of their nodes contains a single digit. Add the two numbers and return
the sum as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Example 1:
Input: l1 = [7,2,4,3], l2 = [5,6,4]
Output: [7,8,0,7]

Example 2:
Input: l1 = [2,4,3], l2 = [5,6,4]
Output: [8,0,7]

Example 3:
Input: l1 = [0], l2 = [0]
Output: [0]


Constraints:
The number of nodes in each linked list is in the range [1, 100].
0 <= Node.val <= 9
It is guaranteed that the list represents a number that does not have leading zeros.


Follow up: Could you solve it without reversing the input lists?

time consuming: *****
*/

func addTwoNumbers445(l1 *ListNode, l2 *ListNode) *ListNode {
	stack1, stack2 := make([]int, 0), make([]int, 0)

	current1, current2 := l1, l2

	for current1 != nil {
		stack1 = append(stack1, current1.Val)
		current1 = current1.Next
	}

	for current2 != nil {
		stack2 = append(stack2, current2.Val)
		current2 = current2.Next
	}

	var res *ListNode
	carry := 0

	for len(stack1) != 0 || len(stack2) != 0 {
		sum := 0
		if len(stack1) != 0 {
			sum += stack1[len(stack1)-1]
			stack1 = stack1[:len(stack1)-1]
		}
		if len(stack2) != 0 {
			sum += stack2[len(stack2)-1]
			stack2 = stack2[:len(stack2)-1]
		}

		sum += carry
		carry = 0

		if sum > 9 {
			sum -= 10
			carry++
		}

		if res == nil {
			res = &ListNode{
				Val: sum,
			}
		} else {
			newList := &ListNode{
				Val:  sum,
				Next: res,
			}
			res = newList
		}
	}

	// don't miss this edge case, for instance [5], [5]
	if carry != 0 {
		return &ListNode{
			Val:  1,
			Next: res,
		}
	}

	return res
}
