package linkedlist

/*
	Example 1:
	Input: l1 = [2,4,3], l2 = [5,6,4]
	Output: [7,0,8]
	Explanation: 342 + 465 = 807.

	Example 2:
	Input: l1 = [0], l2 = [0]
	Output: [0]

	Example 3:
	Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
	Output: [8,9,9,9,0,0,0,1]
*/

func addTwoNumbers0002(l1 *ListNode, l2 *ListNode) *ListNode {
	// end the recursion
	if l1 == nil && l2 == nil {
		return nil
	}

	if l1 == nil {
		if l2.Val > 9 {
			return &ListNode{
				Val:  l2.Val - 10,
				Next: addTwoNumbers0002(&ListNode{Val: 1}, l2.Next),
			}
		}
		return &ListNode{
			Val:  l2.Val,
			Next: addTwoNumbers0002(nil, l2.Next),
		}
	}

	if l2 == nil {
		if l1.Val > 9 {
			return &ListNode{
				Val:  l1.Val - 10,
				Next: addTwoNumbers0002(l1.Next, &ListNode{Val: 1}),
			}
		}
		return &ListNode{
			Val:  l1.Val,
			Next: addTwoNumbers0002(l1.Next, nil),
		}
	}

	if sum := l1.Val + l2.Val; sum <= 9 {
		return &ListNode{
			Val:  sum,
			Next: addTwoNumbers0002(l1.Next, l2.Next),
		}
	}

	sum := l1.Val + l2.Val - 10

	if l1.Next != nil {
		l1.Next.Val++
		return &ListNode{
			Val:  sum,
			Next: addTwoNumbers0002(l1.Next, l2.Next),
		}
	}

	if l2.Next != nil {
		l2.Next.Val++
		return &ListNode{
			Val:  sum,
			Next: addTwoNumbers0002(l1.Next, l2.Next),
		}
	}

	return &ListNode{
		Val:  sum,
		Next: &ListNode{Val: 1},
	}
}
