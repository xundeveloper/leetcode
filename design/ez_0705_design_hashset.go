package design

/*
	Design a HashSet without using any built-in hash table libraries.

	Implement MyHashSet class:
	void add(key) Inserts the value key into the HashSet.
	bool contains(key) Returns whether the value key exists in the HashSet or not.
	void remove(key) Removes the value key in the HashSet. If key does not exist in the HashSet, do nothing.

	Example 1:
	Input
	["MyHashSet", "add", "add", "contains", "contains", "add", "contains", "remove", "contains"]
	[[], [1], [2], [1], [3], [2], [2], [2], [2]]
	Output
	[null, null, null, true, false, null, true, null, false]

	Explanation
	MyHashSet myHashSet = new MyHashSet();
	myHashSet.add(1);      // set = [1]
	myHashSet.add(2);      // set = [1, 2]
	myHashSet.contains(1); // return True
	myHashSet.contains(3); // return False, (not found)
	myHashSet.add(2);      // set = [1, 2]
	myHashSet.contains(2); // return True
	myHashSet.remove(2);   // set = [1]
	myHashSet.contains(2); // return False, (already removed)

	Constraints:
	0 <= key <= 10^6
	At most 10^4 calls will be made to add, remove, and contains.
*/

// type MyHashSet struct {
// 	size int
// 	head *ListNode
// }

// func Constructor() MyHashSet {
// 	return MyHashSet{}
// }

// func (this *MyHashSet) Add(key int) {
// 	if this.size == 0 {
// 		this.head = &ListNode{
// 			Val:  key,
// 			Next: nil,
// 		}
// 		this.size++
// 	} else {
// 		current := this.head
// 		for current != nil {
// 			if current.Val == key {
// 				return
// 			}
// 			if current.Next != nil {
// 				current = current.Next
// 			} else {
// 				current.Next = &ListNode{
// 					Val:  key,
// 					Next: nil,
// 				}
// 				this.size++
// 			}

// 		}
// 	}
// }

// func (this *MyHashSet) Remove(key int) {
// 	var prev *ListNode
// 	current := this.head

// 	for current != nil {
// 		if current.Val == key {
// 			// perform deletion
// 			if prev == nil {
// 				this.head = current.Next
// 			} else {
// 				prev.Next = current.Next
// 			}
// 			this.size--
// 			break
// 		}
// 		prev, current = current, current.Next
// 	}
// }

// func (this *MyHashSet) Contains(key int) bool {
// 	current := this.head
// 	for current != nil {
// 		if current.Val == key {
// 			return true
// 		}
// 		current = current.Next
// 	}
// 	return false
// }
