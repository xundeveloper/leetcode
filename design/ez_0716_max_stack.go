package design

/*
	Design a max stack data structure that supports the stack operations and supports finding the stack's maximum element.

	Implement the MaxStack class:
	MaxStack() Initializes the stack object.
	void push(int x) Pushes element x onto the stack.
	int pop() Removes the element on top of the stack and returns it.
	int top() Gets the element on the top of the stack without removing it.
	int peekMax() Retrieves the maximum element in the stack without removing it.
	int popMax() Retrieves the maximum element in the stack and removes it. If there is more than one maximum element, only remove the top-most one.
*/

// 1 -> 2 -
// type MaxStack struct {
// 	head, maxStack *ListNode
// }

// func Constructor() MaxStack {
// 	return MaxStack{}
// }

// func (this *MaxStack) Push(x int) {
// 	if this.head == nil {
// 		this.head = &ListNode{Val: x}
// 		this.maxStack = &ListNode{Val: x}
// 	} else {
// 		node := &ListNode{Val: x}
// 		this.head, node.Next = node, this.head
// 		if x > this.maxStack.Val {

// 		}
// 	}
// }

// func (this *MaxStack) Pop() int {
// 	res := this.head.Val
// 	this.head = this.head.Next
// 	return res
// }

// func (this *MaxStack) Top() int {
// 	return this.head.Val
// }

// func (this *MaxStack) PeekMax() int {

// }

// func (this *MaxStack) PopMax() int {

// }
