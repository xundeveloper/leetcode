package twopinters

/*
Given a string s, reverse the order of characters in each word within a sentence while still preserving whitespace and initial word order.

Example 1:
Input: s = "Let's take LeetCode contest"
Output: "s'teL ekat edoCteeL tsetnoc"

Example 2:
Input: s = "God Ding"
Output: "doG gniD"

Constraints:
1 <= s.length <= 5 * 104
s contains printable ASCII characters.
s does not contain any leading or trailing spaces.
There is at least one word in s.
All the words in s are separated by a single space.
*/

func reverseWords557(s string) string {
	res, bytes := "", []byte{}
	for i := 0; i < len(s); i++ {
		if b := s[i]; b != 32 {
			bytes = append(bytes, b)
		} else {
			res += helper557(string(bytes)) + " "
			bytes = bytes[:0]
		}
	}

	res += helper557(string(bytes)) // don't miss the last word of string

	return res
}

func helper557(s string) string {
	i, j := 0, len(s)-1
	bytes := []byte(s)
	for i < j {
		bytes[i], bytes[j] = bytes[j], bytes[i]
		i++
		j--
	}
	return string(bytes)
}
