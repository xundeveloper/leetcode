package twopinters

import "math"

/*
phrase is a palindrome if, after converting all uppercase letters into lowercase letters and removing all non-alphanumeric characters, it reads the same forward and backward. Alphanumeric characters include letters and numbers.
Given a string s, return true if it is a palindrome, or false otherwise.

Example 1:
Input: s = "A man, a plan, a canal: Panama"
Output: true
Explanation: "amanaplanacanalpanama" is a palindrome.

Example 2:
Input: s = "race a car"
Output: false
Explanation: "raceacar" is not a palindrome.

Example 3:
Input: s = " "
Output: true
Explanation: s is an empty string "" after removing non-alphanumeric characters.
Since an empty string reads the same forward and backward, it is a palindrome.


Constraints:
1 <= s.length <= 2 * 105
s consists only of printable ASCII characters.
*/

func isPalindrome125(s string) bool {
	if len(s) == 0 {
		return true
	}
	start, end := 0, len(s)-1
	for start < end {
		startVal, endVal := int(s[start]), int(s[end])

		if !isAlpha125(startVal) && !isNumber125(startVal) {
			start++
			continue
		}

		if !isAlpha125(endVal) && !isNumber125(endVal) {
			end--
			continue
		}

		if !isSameAlphaCase126(startVal, endVal) {
			return false
		}

		start++
		end--
	}
	return true
}

func isAlpha125(ascii int) bool {
	return (ascii <= 90 && ascii >= 65) || (ascii <= 122 && ascii >= 97)
}

func isNumber125(ascii int) bool {
	return ascii <= 57 && ascii >= 48
}

func isSameAlphaCase126(letter1, letter2 int) bool {
	if letter1 == letter2 {
		return true
	}

	if isAlpha125(letter1) && isAlpha125(letter2) {
		gap := math.Max(float64(letter1), float64(letter2)) - math.Min(float64(letter1), float64(letter2))
		return gap == 32
	}

	return false
}
