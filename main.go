package main

import (
	"fmt"

	"gitlab.com/leetcode/linkedlist"
)

func main() {
	node := &linkedlist.ListNode{
		Val:  1,
		Next: nil,
	}
	fmt.Printf("node is %v", *node)
}
